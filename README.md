# TASK MANAGER

## DEVELOPER INFO

name: Dmitriy Subbotin

email: subbotin.dmitriy94@gmail.com

email: dsubbotin@tsconsulting.com

## HARDWARE

CPU: i5

RAM: 8Gb

SSD: 110Gb

## SOFTWARE

OS: Windows 10 (2004)

JDK: 15.0.1

## APPLICATION BUILD

```bash
mvn clean install
```

## APPLICATION RUN

```
java -jar ./task-manager.jar
```

## SCREENSHOTS

https://cloud.mail.ru/public/4dGq/BhfwMTwBx/
