package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectTaskService;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String projectId,
                                  final String taskId) throws EmptyIdException,
            ProjectNotFoundException,
            TaskNotFoundException {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existById(projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existById(taskId)) throw new TaskNotFoundException();
        return taskRepository.bindTaskToProjectById(projectId, taskId);
    }

    @Override
    public Task unbindTaskFromProject(final String projectId,
                                      final String taskId) throws EmptyIdException,
            ProjectNotFoundException,
            TaskNotFoundException {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existById(projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existById(taskId)) throw new TaskNotFoundException();
        return taskRepository.unbindTaskById(taskId);
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String id) throws EmptyIdException,
            ProjectNotFoundException,
            TaskNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existById(id)) throw new ProjectNotFoundException();
        return taskRepository.findAllByProjectId(id);
    }

    @Override
    public Project removeProjectById(final String id) throws EmptyIdException,
            ProjectNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (!projectRepository.existById(id)) throw new ProjectNotFoundException();
        taskRepository.removeAllTaskByProjectId(id);
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeProjectByIndex(final int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
        final Project project = projectRepository.findByIndex(index);
        taskRepository.removeAllTaskByProjectId(project.getId());
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeProjectByName(final String name) throws EmptyNameException,
            ProjectNotFoundException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findByName(name);
        taskRepository.removeAllTaskByProjectId(project.getId());
        return projectRepository.removeByName(name);
    }

}
