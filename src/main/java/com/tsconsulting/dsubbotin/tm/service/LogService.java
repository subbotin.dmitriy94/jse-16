package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.ILogService;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

public class LogService implements ILogService {

    private final static String FILE_NAME = "/logger.properties";

    private final static String COMMANDS = "COMMANDS";

    private final static String COMMANDS_FILE = "./commands.log";

    private final static String ERRORS = "ERRORS";

    private final static String ERRORS_FILE = "./errors.log";


    private final static Handler CONSOLE_HANDLER = getConsoleHandler();

    private final LogManager manager = LogManager.getLogManager();

    private final Logger root = Logger.getLogger("");

    private final Logger commands = Logger.getLogger(COMMANDS);

    private final Logger errors = Logger.getLogger(ERRORS);

    {
        init();
        registry(commands, COMMANDS_FILE, false);
        registry(errors, ERRORS_FILE, true);
    }

    private static Handler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init() {
        try {
            final InputStream inputStream = LogService.class.getResourceAsStream(FILE_NAME);
            manager.readConfiguration(inputStream);
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void registry(final Logger logger, final String fileName, final boolean isConsole) {
        try {
            if (isConsole) logger.addHandler(CONSOLE_HANDLER);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (IOException e) {
            root.severe(e.getMessage());
        }
    }

    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        commands.info(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        errors.log(Level.SEVERE, e.getMessage(), e);
    }

}
