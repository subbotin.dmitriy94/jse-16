package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIndexException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name) {
        if (name == null || name.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name,
                       final String description) throws EmptyNameException,
            EmptyDescriptionException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return Collections.emptyList();
        return projectRepository.findAll(comparator);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findById(final String id) throws EmptyIdException,
            ProjectNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findById(id);
    }

    @Override
    public Project findByIndex(final int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project findByName(final String name) throws EmptyNameException,
            ProjectNotFoundException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(name);
    }

    @Override
    public Project updateById(final String id,
                              final String name,
                              final String description) throws EmptyIdException,
            EmptyNameException,
            ProjectNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.updateBuyId(id, name, description);
    }

    @Override
    public Project updateByIndex(final int index,
                                 final String name,
                                 final String description) throws EmptyIndexException,
            EmptyNameException,
            IndexIncorrectException {
        if (index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.updateBuyIndex(index, name, description);
    }

    @Override
    public Project startById(final String id) throws EmptyIdException,
            ProjectNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.startById(id);
    }

    @Override
    public Project startByIndex(final int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
        return projectRepository.startByIndex(index);
    }

    @Override
    public Project startByName(final String name) throws EmptyNameException,
            ProjectNotFoundException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.startByName(name);
    }

    @Override
    public Project finishById(final String id) throws EmptyIdException,
            ProjectNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.finishById(id);
    }

    @Override
    public Project finishByIndex(final int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project finishByName(final String name) throws EmptyNameException,
            ProjectNotFoundException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.finishByName(name);
    }

    @Override
    public Project updateStatusById(final String id,
                                    final Status status) throws EmptyIdException,
            ProjectNotFoundException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.updateStatusById(id, status);
    }

    @Override
    public Project updateStatusByIndex(final int index,
                                       final Status status) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
        return projectRepository.updateStatusByIndex(index, status);
    }

    @Override
    public Project updateStatusByName(final String name,
                                      final Status status) throws EmptyNameException,
            ProjectNotFoundException {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.updateStatusByName(name, status);
    }

}
