package com.tsconsulting.dsubbotin.tm.comparator;

import com.tsconsulting.dsubbotin.tm.api.entity.IHasName;

import java.util.Comparator;

public final class ComparatorByName implements Comparator<IHasName> {

    public static final ComparatorByName INSTANCE = new ComparatorByName();

    private ComparatorByName() {
    }

    @Override
    public int compare(IHasName o1, IHasName o2) {
        return o1.getName().compareTo(o2.getName());
    }

}
