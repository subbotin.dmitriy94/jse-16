package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        final List<Task> taskList = new ArrayList<>(tasks);
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public boolean existById(final String id) throws TaskNotFoundException {
        return findById(id) != null;
    }

    @Override
    public Task findById(final String id) throws TaskNotFoundException {
        for (Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public Task findByIndex(final int index) throws IndexIncorrectException {
        if (tasks.size() - 1 < index) throw new IndexIncorrectException();
        return tasks.get(index);
    }

    @Override
    public Task findByName(final String name) throws TaskNotFoundException {
        for (Task task : tasks) {
            if (name.equals(task.getName())) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public Task removeById(final String id) throws TaskNotFoundException {
        final Task task = findById(id);
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final int index) throws IndexIncorrectException {
        final Task task = findByIndex(index);
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String name) throws TaskNotFoundException {
        final Task task = findByName(name);
        tasks.remove(task);
        return task;
    }

    @Override
    public Task updateBuyId(final String id,
                            final String name,
                            final String description) throws TaskNotFoundException {
        final Task task = findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateBuyIndex(final int index,
                               final String name,
                               final String description) throws IndexIncorrectException {
        final Task task = findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startById(final String id) throws TaskNotFoundException {
        final Task task = findById(id);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByIndex(final int index) throws IndexIncorrectException {
        final Task task = findByIndex(index);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByName(final String name) throws TaskNotFoundException {
        final Task task = findByName(name);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task finishById(final String id) throws TaskNotFoundException {
        final Task task = findById(id);
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(final int index) throws IndexIncorrectException {
        final Task task = findByIndex(index);
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(final String name) throws TaskNotFoundException {
        final Task task = findByName(name);
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task updateStatusById(final String id,
                                 final Status status) throws TaskNotFoundException {
        final Task task = findById(id);
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task updateStatusByIndex(final int index,
                                    final Status status) throws IndexIncorrectException {
        final Task task = findByIndex(index);
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task updateStatusByName(final String name,
                                   final Status status) throws TaskNotFoundException {
        final Task task = findByName(name);
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task bindTaskToProjectById(final String projectId,
                                      final String taskId) throws TaskNotFoundException {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String id) throws TaskNotFoundException {
        final Task task = findById(id);
        task.setProjectId(null);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String id) throws TaskNotFoundException {
        final List<Task> taskList = new ArrayList<>();
        for (Task task : tasks) {
            if (id.equals(task.getProjectId())) taskList.add(task);
        }
        if (taskList.size() == 0) throw new TaskNotFoundException();
        return taskList;
    }

    @Override
    public void removeAllTaskByProjectId(final String id) {
        for (Task task : tasks) {
            if (id.equals(task.getProjectId())) task.setProjectId(null);
        }
    }

}
