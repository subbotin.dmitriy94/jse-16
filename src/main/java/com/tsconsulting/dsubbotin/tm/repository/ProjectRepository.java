package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    public List<Project> findAll(final Comparator<Project> comparator) {
        final List<Project> projectList = new ArrayList<>(projects);
        projectList.sort(comparator);
        return projectList;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public boolean existById(final String id) throws ProjectNotFoundException {
        return findById(id) != null;
    }

    @Override
    public Project findById(final String id) throws ProjectNotFoundException {
        for (Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public Project findByIndex(final int index) throws IndexIncorrectException {
        if (projects.size() - 1 < index) throw new IndexIncorrectException();
        return projects.get(index);
    }

    @Override
    public Project findByName(final String name) throws ProjectNotFoundException {
        for (Project project : projects) {
            if (name.equals(project.getName())) return project;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public Project removeById(final String id) throws ProjectNotFoundException {
        final Project project = findById(id);
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final int index) throws IndexIncorrectException {
        final Project project = findByIndex(index);
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeByName(final String name) throws ProjectNotFoundException {
        final Project project = findByName(name);
        projects.remove(project);
        return project;
    }

    @Override
    public Project updateBuyId(final String id,
                               final String name,
                               final String description) throws ProjectNotFoundException {
        final Project project = findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateBuyIndex(final int index,
                                  final String name,
                                  final String description) throws IndexIncorrectException {
        final Project project = findByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startById(final String id) throws ProjectNotFoundException {
        final Project project = findById(id);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByIndex(final int index) throws IndexIncorrectException {
        final Project project = findByIndex(index);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByName(final String name) throws ProjectNotFoundException {
        final Project project = findByName(name);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project finishById(final String id) throws ProjectNotFoundException {
        final Project project = findById(id);
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByIndex(final int index) throws IndexIncorrectException {
        final Project project = findByIndex(index);
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByName(final String name) throws ProjectNotFoundException {
        final Project project = findByName(name);
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project updateStatusById(final String id,
                                    final Status status) throws ProjectNotFoundException {
        final Project project = findById(id);
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project updateStatusByIndex(final int index,
                                       final Status status) throws IndexIncorrectException {
        final Project project = findByIndex(index);
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project updateStatusByName(final String name,
                                      final Status status) throws ProjectNotFoundException {
        final Project project = findByName(name);
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
        return project;
    }

}