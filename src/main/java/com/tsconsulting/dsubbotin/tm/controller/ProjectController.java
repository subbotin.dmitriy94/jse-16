package com.tsconsulting.dsubbotin.tm.controller;

import com.tsconsulting.dsubbotin.tm.api.controller.IProjectController;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIndexException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownStatusException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() throws EmptyNameException,
            EmptyDescriptionException {
        showMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        showMessage("Enter description:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        showMessage("[Create]");
    }

    @Override
    public void showProjects() throws ProjectNotFoundException {
        showMessage("Enter sort:");
        showMessage(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Project> projects;
        boolean containsFlag = Arrays.asList(Sort.values()).contains(sort);
        if (sort == null || sort.isEmpty() || !containsFlag) projects = projectService.findAll();
        else {
            Sort sortType = Sort.valueOf(sort);
            showMessage(sortType.getDisplayName());
            projects = projectService.findAll(sortType.getComparator());
        }
        if (projects == null || projects.size() == 0) throw new ProjectNotFoundException();
        int index = 1;
        for (Project project : projects) showMessage(index++ + ". " + project);
    }

    @Override
    public void clearProjects() {

        projectService.clear();
        showMessage("[Clear]");
    }

    @Override
    public void showById() throws EmptyIdException,
            ProjectNotFoundException {
        showMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        showProject(project);
    }

    @Override
    public void showByIndex() throws IndexIncorrectException,
            ProjectNotFoundException {
        showMessage("Enter index:");
        try {
            final int index = TerminalUtil.nextNumber() - 1;
            final Project project = projectService.findByIndex(index);
            showProject(project);
        } catch (NumberFormatException e) {
            throw new IndexIncorrectException();
        }
    }

    @Override
    public void showByName() throws EmptyNameException,
            ProjectNotFoundException {
        showMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.findByName(name);
        showProject(project);
    }

    @Override
    public void updateById() throws EmptyIdException,
            EmptyNameException,
            ProjectNotFoundException {
        showMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        Project foundProject = projectService.findById(id);
        showMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        showMessage("Enter description:");
        final String description = TerminalUtil.nextLine();
        Project updatedProject = projectService.updateById(id, name, description);
        showMessage("[Updated project]");
    }

    @Override
    public void updateByIndex() throws IndexIncorrectException,
            EmptyIndexException,
            EmptyNameException {
        showMessage("Enter index:");
        try {
            final int index = TerminalUtil.nextNumber() - 1;
            Project foundProject = projectService.findByIndex(index);
            showMessage("Enter name:");
            final String name = TerminalUtil.nextLine();
            showMessage("Enter description:");
            final String description = TerminalUtil.nextLine();
            Project updatedProject = projectService.updateByIndex(index, name, description);
            showMessage("[Updated project]");

        } catch (NumberFormatException e) {
            throw new IndexIncorrectException();
        }
    }

    @Override
    public void startById() throws EmptyIdException,
            ProjectNotFoundException {
        showMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.startById(id);
    }

    @Override
    public void startByIndex() throws IndexIncorrectException {
        showMessage("Enter index:");
        try {
            final int index = TerminalUtil.nextNumber() - 1;
            final Project project = projectService.startByIndex(index);
        } catch (NumberFormatException e) {
            throw new IndexIncorrectException();
        }
    }

    @Override
    public void startByName() throws EmptyNameException,
            ProjectNotFoundException {
        showMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.startByName(name);
    }

    @Override
    public void finishById() throws EmptyIdException,
            ProjectNotFoundException {
        showMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.finishById(id);
    }

    @Override
    public void finishByIndex() throws IndexIncorrectException {
        showMessage("Enter index:");
        try {
            final int index = TerminalUtil.nextNumber() - 1;
            final Project project = projectService.finishByIndex(index);
        } catch (NumberFormatException e) {
            throw new IndexIncorrectException();
        }

    }

    @Override
    public void finishByName() throws EmptyNameException,
            ProjectNotFoundException {
        showMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectService.finishByName(name);
    }

    @Override
    public void updateStatusById() throws EmptyIdException,
            ProjectNotFoundException, UnknownStatusException {
        showMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        Project foundProject = projectService.findById(id);
        showMessage("Enter status:");
        showMessage(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        try {
            Status status = Status.valueOf(statusValue);
            final Project updatedStatusProject = projectService.updateStatusById(id, status);
            showMessage("[Updated project status]");
        } catch (IllegalArgumentException e) {
            throw new UnknownStatusException();
        }
    }

    @Override
    public void updateStatusByIndex() throws IndexIncorrectException,
            UnknownStatusException {
        showMessage("Enter index:");
        try {
            final int index = TerminalUtil.nextNumber() - 1;
            Project foundProject = projectService.findByIndex(index);
            showMessage("Enter status:");
            showMessage(Arrays.toString(Status.values()));
            final String statusValue = TerminalUtil.nextLine();
            try {
                Status status = Status.valueOf(statusValue);
                final Project updatedStatusProject = projectService.updateStatusByIndex(index, status);
                showMessage("[Updated project status]");
            } catch (IllegalArgumentException e) {
                throw new UnknownStatusException();
            }
        } catch (NumberFormatException e) {
            throw new IndexIncorrectException();
        }

    }

    @Override
    public void updateStatusByName() throws EmptyNameException,
            ProjectNotFoundException,
            UnknownStatusException {
        showMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        Project foundProject = projectService.findByName(name);
        showMessage("Enter status:");
        showMessage(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        try {
            Status status = Status.valueOf(statusValue);
            final Project updatedStatusProject = projectService.updateStatusByName(name, status);
            showMessage("[Updated project status]");
        } catch (IllegalArgumentException e) {
            throw new UnknownStatusException();
        }
    }

    private void showMessage(String value) {
        System.out.println(value);
    }

    private void showProject(final Project project) throws ProjectNotFoundException {
        if (project == null) throw new ProjectNotFoundException();
        showMessage("Id: " + project.getId() + "\n" +
                "Name: " + project.getName() + "\n" +
                "Description: " + project.getDescription() + "\n" +
                "Status: " + project.getStatus().getDisplayName() + "\n" +
                "Create date: " + project.getCreateDate() + "\n" +
                "Start date: " + project.getStartDate()
        );
    }

}