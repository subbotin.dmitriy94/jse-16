package com.tsconsulting.dsubbotin.tm.controller;

import com.tsconsulting.dsubbotin.tm.api.controller.ICommandController;
import com.tsconsulting.dsubbotin.tm.api.service.ICommandService;
import com.tsconsulting.dsubbotin.tm.model.Command;
import com.tsconsulting.dsubbotin.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    @Override
    public void displayAbout() {
        System.out.println("Developer: Dmitriy Subbotin");
        System.out.println("E-Mail: dsubbotin@tsconsulting.com");
    }

    @Override
    public void displayVersion() {
        System.out.println("1.16.1");
    }

    @Override
    public void displayInfo() {
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    @Override
    public void displayHelp() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            displayValue(command.toString());
        }
    }

    @Override
    public void displayCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            displayValue(command.getName());
        }
    }

    @Override
    public void displayArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            displayValue(command.getArgument());
        }
    }

    private void displayValue(final String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
