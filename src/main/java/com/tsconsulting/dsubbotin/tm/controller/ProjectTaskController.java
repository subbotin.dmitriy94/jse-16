package com.tsconsulting.dsubbotin.tm.controller;

import com.tsconsulting.dsubbotin.tm.api.controller.IProjectTaskController;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectService;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectTaskService;
import com.tsconsulting.dsubbotin.tm.api.service.ITaskService;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectService projectService;

    private final ITaskService taskService;

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(IProjectService projectService,
                                 ITaskService taskService,
                                 IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() throws EmptyIdException,
            ProjectNotFoundException,
            TaskNotFoundException {
        showMessage("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        showMessage("Enter task id:");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskId);
        Task taskTiedToProject = projectTaskService.bindTaskToProject(projectId, taskId);
        showMessage("[Task tied to project]");
    }

    @Override
    public void unbindTaskFromProject() throws EmptyIdException,
            ProjectNotFoundException,
            TaskNotFoundException {
        showMessage("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        showMessage("Enter task id:");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskId);
        Task taskUntiedToProject = projectTaskService.unbindTaskFromProject(projectId, taskId);
        showMessage("[Task untied to project]");
    }

    @Override
    public void findAllTasksByProjectId() throws EmptyIdException,
            ProjectNotFoundException,
            TaskNotFoundException {
        showMessage("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        List<Task> tasks = projectTaskService.findAllTasksByProjectId(projectId);
        int index = 1;
        for (Task task : tasks) System.out.println(index++ + ". " + task.toString());
    }

    @Override
    public void removeProjectById() throws EmptyIdException,
            ProjectNotFoundException {
        showMessage("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        final Project removedProject = projectTaskService.removeProjectById(projectId);
        showMessage("[Project removed]");
    }

    @Override
    public void removeProjectByIndex() throws IndexIncorrectException {
        showMessage("Enter index:");
        try {
            final int index = TerminalUtil.nextNumber() - 1;
            final Project project = projectTaskService.removeProjectByIndex(index);
            showMessage("[Project removed]");
        } catch (NumberFormatException e) {
            throw new IndexIncorrectException();
        }
    }

    @Override
    public void removeProjectByName() throws ProjectNotFoundException,
            EmptyNameException {
        showMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Project project = projectTaskService.removeProjectByName(name);
        showMessage("[Project removed]");
    }

    private void showMessage(String message) {
        System.out.println(message);
    }

}
