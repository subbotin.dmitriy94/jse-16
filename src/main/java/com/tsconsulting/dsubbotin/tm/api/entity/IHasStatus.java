package com.tsconsulting.dsubbotin.tm.api.entity;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
