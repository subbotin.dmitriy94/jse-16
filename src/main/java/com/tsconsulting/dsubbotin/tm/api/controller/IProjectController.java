package com.tsconsulting.dsubbotin.tm.api.controller;

import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIndexException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownStatusException;

public interface IProjectController {

    void createProject() throws EmptyNameException, EmptyDescriptionException;

    void showProjects() throws ProjectNotFoundException;

    void clearProjects();

    void showById() throws EmptyIdException, ProjectNotFoundException;

    void showByIndex() throws IndexIncorrectException, ProjectNotFoundException;

    void showByName() throws EmptyNameException, ProjectNotFoundException;

    void updateById() throws EmptyIdException, EmptyNameException, ProjectNotFoundException;

    void updateByIndex() throws IndexIncorrectException, EmptyIndexException, EmptyNameException;

    void startById() throws EmptyIdException, ProjectNotFoundException;

    void startByIndex() throws IndexIncorrectException;

    void startByName() throws EmptyNameException, ProjectNotFoundException;

    void finishById() throws EmptyIdException, ProjectNotFoundException;

    void finishByIndex() throws IndexIncorrectException;

    void finishByName() throws EmptyNameException, ProjectNotFoundException;

    void updateStatusById() throws EmptyIdException, ProjectNotFoundException, UnknownStatusException;

    void updateStatusByIndex() throws IndexIncorrectException, UnknownStatusException;

    void updateStatusByName() throws EmptyNameException, ProjectNotFoundException, UnknownStatusException;

}
