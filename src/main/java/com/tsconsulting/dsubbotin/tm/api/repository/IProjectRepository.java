package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    void add(final Project project);

    void remove(final Project project);

    List<Project> findAll();

    List<Project> findAll(final Comparator<Project> comparator);

    void clear();

    boolean existById(final String id) throws ProjectNotFoundException;

    Project findById(final String id) throws ProjectNotFoundException;

    Project findByIndex(final int index) throws IndexIncorrectException;

    Project findByName(final String name) throws ProjectNotFoundException;

    Project removeById(final String id) throws ProjectNotFoundException;

    Project removeByIndex(final int index) throws IndexIncorrectException;

    Project removeByName(final String name) throws ProjectNotFoundException;

    Project updateBuyId(final String id, final String name, final String description) throws ProjectNotFoundException;

    Project updateBuyIndex(final int index, final String name, final String description) throws IndexIncorrectException;

    Project startById(final String id) throws ProjectNotFoundException;

    Project startByIndex(final int index) throws IndexIncorrectException;

    Project startByName(final String name) throws ProjectNotFoundException;

    Project finishById(final String id) throws ProjectNotFoundException;

    Project finishByIndex(final int index) throws IndexIncorrectException;

    Project finishByName(final String name) throws ProjectNotFoundException;

    Project updateStatusById(final String id, final Status status) throws ProjectNotFoundException;

    Project updateStatusByIndex(final int index, final Status status) throws IndexIncorrectException;

    Project updateStatusByName(final String name, final Status status) throws ProjectNotFoundException;

}
