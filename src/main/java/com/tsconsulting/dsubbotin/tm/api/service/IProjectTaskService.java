package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Task bindTaskToProject(final String projectId, final String taskId) throws ProjectNotFoundException, TaskNotFoundException, EmptyIdException;

    Task unbindTaskFromProject(final String projectId, final String taskId) throws ProjectNotFoundException, TaskNotFoundException, EmptyIdException;

    List<Task> findAllTasksByProjectId(final String id) throws ProjectNotFoundException, EmptyIdException, TaskNotFoundException;

    Project removeProjectById(final String id) throws ProjectNotFoundException, EmptyIdException;

    Project removeProjectByIndex(final int index) throws IndexIncorrectException;

    Project removeProjectByName(final String name) throws ProjectNotFoundException, EmptyNameException;

}
