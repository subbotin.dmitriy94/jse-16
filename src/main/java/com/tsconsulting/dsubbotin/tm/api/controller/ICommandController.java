package com.tsconsulting.dsubbotin.tm.api.controller;

public interface ICommandController {

    void displayWelcome();

    void displayAbout();

    void displayVersion();

    void displayInfo();

    void displayHelp();

    void displayCommands();

    void displayArguments();

    void exit();

}
