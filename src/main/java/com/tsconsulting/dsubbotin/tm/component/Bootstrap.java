package com.tsconsulting.dsubbotin.tm.component;

import com.tsconsulting.dsubbotin.tm.api.controller.ICommandController;
import com.tsconsulting.dsubbotin.tm.api.controller.IProjectController;
import com.tsconsulting.dsubbotin.tm.api.controller.IProjectTaskController;
import com.tsconsulting.dsubbotin.tm.api.controller.ITaskController;
import com.tsconsulting.dsubbotin.tm.api.repository.ICommandRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.*;
import com.tsconsulting.dsubbotin.tm.constant.ArgumentConst;
import com.tsconsulting.dsubbotin.tm.constant.TerminalConst;
import com.tsconsulting.dsubbotin.tm.controller.CommandController;
import com.tsconsulting.dsubbotin.tm.controller.ProjectController;
import com.tsconsulting.dsubbotin.tm.controller.ProjectTaskController;
import com.tsconsulting.dsubbotin.tm.controller.TaskController;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownArgumentException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownCommandException;
import com.tsconsulting.dsubbotin.tm.repository.CommandRepository;
import com.tsconsulting.dsubbotin.tm.repository.ProjectRepository;
import com.tsconsulting.dsubbotin.tm.repository.TaskRepository;
import com.tsconsulting.dsubbotin.tm.service.*;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final IProjectTaskService projectTaskService =
            new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController =
            new ProjectTaskController(projectService, taskService, projectTaskService);

    private final ILogService logService = new LogService();

    public void run(final String[] args) {
        commandController.displayWelcome();
        initData();
        parseArgs(args);
        process();
    }

    private void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        try {
            parseArg(arg);
        } catch (UnknownArgumentException e) {
            logService.error(e);
            System.exit(1);
        }
        commandController.exit();
    }

    private void parseArg(final String arg) throws UnknownArgumentException {
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            default:
                throw new UnknownArgumentException();
        }
    }

    private void process() {
        logService.debug("Test environment!");
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            command = TerminalUtil.nextLine();
            try {
                logService.command(command);
                parseCommand(command);
                logService.info("Commands '" + command + "' executed!");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }

    private void parseCommand(final String command) throws AbstractException {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.INFO:
                commandController.displayInfo();
                break;
            case TerminalConst.HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showById();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showByIndex();
                break;
            case TerminalConst.TASK_SHOW_BY_NAME:
                taskController.showByName();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeByName();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startByIndex();
                break;
            case TerminalConst.TASK_START_BY_NAME:
                taskController.startByName();
                break;
            case TerminalConst.TASK_FINISH_BY_ID:
                taskController.finishById();
                break;
            case TerminalConst.TASK_FINISH_BY_INDEX:
                taskController.finishByIndex();
                break;
            case TerminalConst.TASK_FINISH_BY_NAME:
                taskController.finishByName();
                break;
            case TerminalConst.TASK_UPDATE_STATUS_BY_ID:
                taskController.updateStatusById();
                break;
            case TerminalConst.TASK_UPDATE_STATUS_BY_INDEX:
                taskController.updateStatusByIndex();
                break;
            case TerminalConst.TASK_UPDATE_STATUS_BY_NAME:
                taskController.updateStatusByName();
                break;
            case TerminalConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case TerminalConst.TASK_LIST_BY_PROJECT_ID:
                projectTaskController.findAllTasksByProjectId();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showByIndex();
                break;
            case TerminalConst.PROJECT_SHOW_BY_NAME:
                projectController.showByName();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectTaskController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectTaskController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectTaskController.removeProjectByName();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_NAME:
                projectController.startByName();
                break;
            case TerminalConst.PROJECT_FINISH_BY_ID:
                projectController.finishById();
                break;
            case TerminalConst.PROJECT_FINISH_BY_INDEX:
                projectController.finishByIndex();
                break;
            case TerminalConst.PROJECT_FINISH_BY_NAME:
                projectController.finishByName();
                break;
            case TerminalConst.PROJECT_UPDATE_STATUS_BY_ID:
                projectController.updateStatusById();
                break;
            case TerminalConst.PROJECT_UPDATE_STATUS_BY_INDEX:
                projectController.updateStatusByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_STATUS_BY_NAME:
                projectController.updateStatusByName();
                break;
            case TerminalConst.EXIT:
                commandController.exit();
                break;
            default:
                throw new UnknownCommandException();
        }
    }

    private void initData() {
        try {
            projectService.create("D_Project_1", "1");
            projectService.create("C_Project_2", "2");
            projectService.create("A_Project_3", "3");
            projectService.create("B_Project_4", "4");
            projectService.create("E_Project_5", "5");

            projectService.startByIndex(1);
            projectService.finishByIndex(2);

            taskService.create("B_Task_1", "1");
            taskService.create("A_Task_2", "2");
            taskService.create("C_Task_3", "3");
            taskService.create("E_Task_4", "4");
            taskService.create("D_Task_5", "5");

            taskService.finishByIndex(3);
            taskService.startByIndex(4);
        } catch (AbstractException e) {
            logService.error(e);
        }
    }

}
