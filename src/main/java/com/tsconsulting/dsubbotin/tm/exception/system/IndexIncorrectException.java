package com.tsconsulting.dsubbotin.tm.exception.system;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Index incorrect entered!");
    }
}
